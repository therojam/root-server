#!/bin/sh

set -e
BASE_DIR=$(dirname $(realpath $0))
. "${BASE_DIR}/base"
cd "${BASE_DIR}/.."

USER_NAME=""
HOSTS="ansible_managed_hosts"

show_help() {
    echo "syntax: $0 [-h] -u <USER> [-l <HOSTS>]"
    echo ""
    echo "\t-h    Help"
    echo "\t-u    User to delete"
    echo "\t-l    Ansible managed host or host group to apply to"
    echo "\t      (default: ansible_managed_hosts)"
    echo "\t-f    Don't ask, apply deletion instantly"
    echo ""
}

while getopts "h?fu:l:" opt ; do
    case "$opt" in
        h)
            show_help
            exit 0
            ;;

        u)
            USER_NAME="$OPTARG"
            ;;

        l)
            HOSTS="$OPTARG"
            ;;

        f)
            FORCE=1
            ;;
    esac
done

if [ -z "${USER_NAME}" ] ; then
    echo "Error: User required"
    show_help
    exit 1
fi

if [ "${USER_NAME}" = "deploy" ] ; then
    echo "I will not remove the deployment user!"
    exit 1
fi

confirm "Delete user ${USER_NAME} from ${HOSTS}?" || exit 1
echo "Removing user ${USER_NAME} from ${HOSTS}"

ansible \
    -b \
    -u deploy \
    -m user \
    -a "name=${USER_NAME} state=absent" \
    "${HOSTS}"

